asgiref==3.5.0
Django==4.0.4
sqlparse==0.4.2
pytz==2021.3
psycopg2-binary
coverage==6.3.2
pip-audit==2.1.1

