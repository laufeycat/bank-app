#!/bin/sh

echo "** RTE mode: $RTE"

python manage.py check
python manage.py makemigrations
python manage.py migrate

case "$RTE" in
	dev )
		echo "** Development mode."
		python manage.py runserver 0.0.0.0:8080
		coverage run --source="." --omit=manage.py manage.py test --verbosity 2
		coverage report -m --fail-under=80
		;;
	test )
		echo "** Test mode."
		coverage run --source="." --omit=manage.py manage.py test --verbosity 2
		coverage report -m --fail-under=80
		;;
	prod ) 
		echo "** Production mode."
		python mange.py check --deploy
		;;
esac
