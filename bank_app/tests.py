from django.test import TestCase, Client
from .models import Account, Customer, Ledger, Store
from .views import index, home, accounts, loans, loan_details, transfer, profile, staff
from .forms import UpdateCustomerForm, createAccount, createCustomer, createUser, UpdateUserForm, TransferForm, LoanForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.urls import reverse
class AccountTestCase(TestCase):
    def setUp(self):
        client = Client()
        testUser = User.objects.create_user(username='johnny',email='j@j.is',first_name='john',last_name='johnsson',password='pass123')
        testUser.set_password('pass123')
        testUser.save()
        Customer(user=testUser).save()
        self.response = self.client.login(username='johnny', password='pass123')
        
        staffUser = User.objects.create_user(username='admin',email='a@a.is',first_name='admin',last_name='admin',password='admin')
        staffUser.save()
        Customer(user=staffUser).save()
        Account(user=staffUser, title='bank').save()
        bankAccount = Account.objects.get(title='bank')

    def test_create_account(self):
        testUser = User.objects.get(username='johnny')
        Account(user=testUser, title='TestCaseAccount').save()
        newAccount = Account.objects.get(title='TestCaseAccount')
        
        account_data = {
            'user': testUser,
            'title': 'accountTest'
        }

        account_form = createAccount(data=account_data)
        self.assertTrue(account_form.is_valid())
        if account_form.is_valid():
            self.response = self.client.post(reverse('bank_app:home'),account_data)
            self.assertTrue(201, self.response.status_code)
            self.assertEqual(self.response.status_code, 200)

        assert newAccount.money == 0
    def test_create_customer(self):
        testUser = User.objects.get(username='johnny')
        Customer(user=testUser)
        assert testUser.customer.customer_rank == 'basic'
        assert testUser.customer.phone_number == ''

    def test_load_index(self):
        self.response = self.client.get('/')
        self.assertTemplateUsed(self.response, 'bank_app/index.html')
        assert self.response.status_code == 200
    def test_home_load(self):
        self.response = self.client.get(reverse('bank_app:home'))
        self.assertTrue(200, self.response.status_code)

    def test_accounts_load(self):
        self.response = self.client.get(reverse('bank_app:accounts'))
        self.assertTrue(200, self.response.status_code)

    def test_loans_load(self):
        self.response = self.client.get(reverse('bank_app:loans'))
        self.assertTrue(200, self.response.status_code)

    def test_profile_load(self):
        self.response = self.client.get(reverse('bank_app:profile'))
        self.assertTrue(200, self.response.status_code)
    
    def test_update_user_form(self):
        new_form_data = {
                'first_name': 'Juliana',
                'last_name': 'Bjort',
                'username': 'juliana',
                'email': 'j@j.is'
                }
        update_form = UpdateUserForm(data=new_form_data)
        self.assertTrue(update_form.is_valid())
        print(update_form.errors)

        self.response = self.client.post(reverse('bank_app:profile'), new_form_data)
        self.assertTrue(201, self.response.status_code)

    def test_transfer_load(self):
        self.response = self.client.get(reverse('bank_app:transfer'))
        self.assertTrue(200, self.response.status_code)


class staffTestCase(TestCase):
    def setUp(self):
        staffUser = User.objects.create_user(username='admin',password='admin',email='a@a.is',first_name='admin',last_name='admin',is_staff=True)
        staffUser.set_password('admin')
        staffUser.save()
        Customer(user=staffUser).save()
        
        self.response = self.client.login(username='admin', password='admin')
        
        testUser = User.objects.create_user(username='johnny',email='j@j.is',first_name='john',last_name='johnsson',password='pass123')
        testUser.save()
        Customer(user=testUser).save()

    def test_staff_GET(self):
        self.response = self.client.get(reverse('bank_app:staff'))
        self.assertTrue(200, self.response.status_code)


    def test_staff_accounts_GET(self):
        self.response = self.client.get(reverse('bank_app:staffNewAccount'))
        self.assertTrue(200, self.response.status_code)

    def test_create_account_form(self):
        testUser = User.objects.get(username='johnny')
        form_data = {
                'title': 'a',
                'user': testUser
                }
        account_form = createAccount(data=form_data)
        self.assertTrue(account_form.is_valid())
        self.response = self.client.post(reverse('bank_app:staffNewAccount'), form_data)
        self.assertTrue(201, self.response.status_code)

    def test_staff_customers_GET(self):
        self.response = self.client.get(reverse('bank_app:staffNewCustomer'))
        self.assertTrue(200, self.response.status_code)
    
    def test_create_user_form(self):
        form_data = {
                'first_name': 'Laufey',
                'last_name': 'Hjaltested',
                'username': 'laufey',
                'email': 'l@l.is',
                'password': 'pass123'
                }
        form = createUser(data=form_data)
        self.assertTrue(form.is_valid())

        customer_form_data = {
                'phone_number': '+4567676767',
                'customer_rank': 'basic',
                }
        form = createCustomer(data=customer_form_data)
        self.assertTrue(form.is_valid())

        self.response = self.client.post(reverse('bank_app:staffNewCustomer'), form_data)
        self.assertTrue(201, self.response.status_code)

    def test_update_customer_form(self):
        testUser = User.objects.get(username='johnny')
        Customer(user=testUser)
        staffUser = User.objects.get(username='admin')
        Customer(user=staffUser)
        form_data = {
                'user': testUser.customer.pk,
                'phone_number': '+4550595859', 
                'customer_rank': 'silver'
                }
        form = UpdateCustomerForm(data=form_data, instance=staffUser)
        self.assertTrue(form.is_valid())
        self.response = self.client.post(reverse('bank_app:staffCustomerView'), form_data)
        self.assertTrue(201, self.response.status_code)

    def test_staff_transfer_GET(self):
        self.response = self.client.get(reverse('bank_app:staffTransfers'))
        self.assertTrue(200, self.response.status_code)

    def test_staff_account_view_GET(self):
        self.response = self.client.get(reverse('bank_app:staffAccountView'))
        self.assertTrue(200, self.response.status_code)

    def test_staff_customer_view_GET(self):
        self.response = self.client.get(reverse('bank_app:staffCustomerView'))
        self.assertTrue(200, self.response.status_code)

class login_app_TestCase(TestCase):
    def setUp(self):
        
        client = Client()
        testUser = User.objects.create_user(username='johnny',email='j@j.is',first_name='john',last_name='johnsson',password='pass123')
        testUser.save()
        Customer(user=testUser).save()

    def test_load_login(self):
        testUser = User.objects.get(username='johnny')
        self.response = self.client.get(reverse('login_app:login'))
        self.assertTemplateUsed(self.response, 'login_app/login.html')
        
        assert self.response.status_code == 200
        
        response = self.client.post(reverse('login_app:login'), data={
            'user': 'johnny',
            'password': 'pass123'
            })
        self.assertTrue(201, self.response.status_code)
        self.assertEqual(response.status_code, 302)
    
    def test_logout(self):
        self.response = self.client.login(username='johnny', password='pass123')
        self.client.logout()
        response = self.client.get(reverse('login_app:login'))
        self.assertEqual(response.status_code, 200)

    def test_load_signup(self):
        self.response = self.client.get(reverse('login_app:sign_up'))
        self.assertTemplateUsed(self.response, 'login_app/sign_up.html')
        assert self.response.status_code == 200
        
        response = self.client.post(reverse('login_app:sign_up'), data={
            'first_name': 'name',
            'last_name': 'last',
            'user': 'test',
            'email': 't@t.is',
            'phone_number': 4555555555,
            'password': 'pass12345',
            'confirm_password': 'pass12345'
        })
        self.assertTrue(201, self.response.status_code)
        self.assertEqual(response.status_code, 302)

        self.assertTemplateUsed(self.response, 'login_app/sign_up.html')
    def test_load_login_worng_password(self):
        response = self.client.post(reverse('login_app:login'), data={
            'user': 'johnny',
            'password': 'pass1234'
            })
        self.assertEqual(response.status_code, 200)
    
    def test_signup_passwords_not_maching(self):
        self.response = self.client.get(reverse('login_app:sign_up'))
        self.assertTemplateUsed(self.response, 'login_app/sign_up.html')
        assert self.response.status_code == 200
        
        response = self.client.post(reverse('login_app:sign_up'), data={
            'first_name': 'name',
            'last_name': 'last',
            'user': 'test',
            'email': 't@t.is',
            'phone_number': 4555555555,
            'password': 'pass12345',
            'confirm_password': 'pass'
        })
        self.assertTrue(201, self.response.status_code)
        self.assertEqual(response.status_code, 200)
    
        
